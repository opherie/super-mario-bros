using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] float moveSpeed = 8f;
    new Rigidbody2D rigidbody2D;
    new Camera camera;
    UnityEngine.Vector2 velocity;
    float inputAxis;
    
    void Awake() 
    {
        rigidbody2D = GetComponent<Rigidbody2D>();   
        camera = Camera.main;
    }

    void Update() 
    {
        HorizontalMovement();
    }

    void HorizontalMovement()
    {
        inputAxis = Input.GetAxis("Horizontal");
        velocity.x = Mathf.MoveTowards(velocity.x, inputAxis * moveSpeed, moveSpeed * Time.deltaTime);
    }

    void FixedUpdate() 
    {
        UnityEngine.Vector2 position = rigidbody2D.position;
        position = position + velocity * Time.fixedDeltaTime;

        Vector2 leftEdge = camera.ScreenToWorldPoint(Vector2.zero);
        Vector2 rightEdge = camera.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
        position.x = Mathf.Clamp(position.x, leftEdge.x + 0.5f, rightEdge.x - 0.5f);

        rigidbody2D.MovePosition(position);
    }
}
